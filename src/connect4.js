/* Uses Code from Randys Demo

1. Initialize the game
    - players
    - board display
    - board model
    - current player tracker
    - click handlers for each column
2. Take player input
    - know which player is dropping a disc
    - which column are we dropping into?
    - is that column already full?
    - drop the disc into the top of the column
3. Check for game end conditions
    - tie
    - win
    - announce that the game is over

*/

const boardModel = [
    [ null, null, null, null, null, null, null ],
    [ null, null, null, null, null, null, null ],
    [ null, null, null, null, null, null, null ],
    [ null, null, null, null, null, null, null ],
    [ null, null, null, null, null, null, null ],
    [ null, null, null, null, null, null, null ]
]

let currentPlayer = 1 // 1 or 2
let numberOfDiscsDropped = 0

// Akil Mcelhannon
const displayMessage = function (message) {
    // Clear out the message div
    // Add new message to div
}


const displayCurrentPlayer = function (currentPlayer) {
    displayMessage("Current player: " + currentPlayer)
}
// Akil Mcelhannon
const isColumnFull = function (colNum) {
    // TODO: Look at the boardModel to determine if col is full
    return false // or true
}

// Ian Waddell
const dropDisc = function (columnNum) {                
    
     let col = document.getElementById("col" + columnNum)

    //Add a disc to the DOM for the current player
    if (currentPlayer === 1) {
        col.innerHTML += '<div class="disc red"></div>'
    } else if (currentPlayer === 2) {
        col.innerHTML += '<div class="disc yellow"></div>'
    }
    
    // Add a disc to the boardModel
    for (let i = col.childElementCount - 1; i >= 0; i--) {
        if (boardModel[i][columnNum] === null) {
            boardModel[i][columnNum] = currentPlayer;
        }
    }
    numberOfDiscsDropped++
}

const isGameOver = function (model) {
    // Check for a win
    // Check for a tie (numberofDiscsDropped === 42)
    return false // false, "tie", "win"
}

const displayTieMessage = function () {
    displayMessage("Tie game!")
}

const displayWinMessage = function () {
    displayMessage("Winner is _____")    
}

// Saxton Cooper
const switchToNextPlayer = function () {
    //     TODO: Toggle currentPlayer variable 1<-->2
    
}

const columnClickHandler = function (eventObj) {
    const selectedCol = eventObj.currentTarget;
    const columnNum = Number(selectedCol.id.slice(-1));

    if (isColumnFull(columnNum)) {
        // display a message saying they can't drop there
    } else {
        dropDisc(columnNum);

        const gameStatus = isGameOver(boardModel)
        if (gameStatus === "tie") {
            displayTieMessage()
        } else if (gameStatus === "win") {
            displayWinMessage()
        } else {
            switchToNextPlayer()
        }
    }
}

const setUpEventListeners = function () {
    document.querySelector('#col0').addEventListener('click', columnClickHandler)
    document.querySelector('#col1').addEventListener('click', columnClickHandler)
    document.querySelector('#col2').addEventListener('click', columnClickHandler)
    document.querySelector('#col3').addEventListener('click', columnClickHandler)
    document.querySelector('#col4').addEventListener('click', columnClickHandler)
    document.querySelector('#col5').addEventListener('click', columnClickHandler)
    document.querySelector('#col6').addEventListener('click', columnClickHandler)
}

const initializeGame = function () {
    setUpEventListeners()
    displayCurrentPlayer(currentPlayer)
}

initializeGame()